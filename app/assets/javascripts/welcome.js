// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

var dateTol;
var dateIg;
var password;
var modositTol;
var modositIg;

function kereses(){
	dateTol=$("#keresTol").val();
	dateIg=$("#keresIg").val();
	hiding();
}

function showMentes(){
	$("#mentesDiv").removeAttr("hidden");
}

function showTorles(){
	$("#torlesDiv").removeAttr("hidden");
}

function showModositas(){
	$("#modositasDiv").removeAttr("hidden");
}

function mentesMentes(){
	hiding();
}

function torlesMentes(){
	hiding();
}

function modositasMentes() {
	modositTol=$("#modositasTol").val();
	modositIg=$("#modositasIg").val();
	hiding();
}

function hiding(){
	$("#mentes").attr("hidden", true);
	$("#torles").attr("hidden", true);
	$("#modositas").attr("hidden", true);
	$("#mentesDiv").attr("hidden", true);
	$("#torlesDiv").attr("hidden", true);
	$("#modositasDiv").attr("hidden", true);
}
