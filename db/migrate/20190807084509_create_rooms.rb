class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.datetime :tol
      t.datetime :ig
      t.string :password

      t.timestamps
    end
  end
end
